# Simplified HPC Storage Prototype: BandGuard

This is an implementation of a simplified HPC storage system to evaluate the ideas proposed in BandGuard.

## Two essential commands you need to build this system:

###  Clean
```
make clean
```
This will clean all the generated thrift interfaces(both in C++ and Java) and generated C++ library files.  Also, the java package built by Apache Maven will be cleaned.

### Compile and Install
```
make install
```
This will generate thrift interfaces(both in C++ and Java) and the C++ library files. Java packages are automatically built by Apache Maven.

## Thrift Generation
```
make clean-thrift
make thrift
```
Usually, to maintain stability of thrift schema and its corresponding generated artifacts, we seldom run these two targets.

## Run
simds is packaged with maven and shell script. You need to first unpack the binary distribution, then run the __server.sh__ in ```bin``` directory.
```
cd release
cd /bin
sh server.sh -help
```
By doing so, you'll get a detailed arg list.
The easiest way is to run servers and clients in a distributed mode, in which case the "-local" parameter should not be passed and -id indicates the ID of the server node, and -srvlist must be provided to make sure each server node knows others.
```
sh server.sh start -db [dbFile] -id [ServerId] -srvlist [s1:p1, s2:p2, …]
```
You can view the output of the program by doing so:
```
cat ../logs/stdout.log
cat ../logs/stderr.log
```
