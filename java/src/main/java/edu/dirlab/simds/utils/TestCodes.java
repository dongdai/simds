package edu.dirlab.simds.utils;

import org.apache.thrift.TException;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TestCodes {

    public static void main(String[] args){
        Runnable ioTask = new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println(this.toString() + " Time: " + System.currentTimeMillis());
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        service.scheduleAtFixedRate(ioTask, 0, 1000, TimeUnit.MILLISECONDS);
    }
}
