package edu.dirlab.simds.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

public class TokenGetPropertyValues {
    InputStream inputStream;

    public HashMap<Integer, Integer> getTokens() throws IOException {

        HashMap<Integer, Integer> result = new HashMap<>();

        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("Token File '" + propFileName + "' not found in the classpath");
            }

            // get the property value and put it in the hashmap
            int jobs = Integer.parseInt(prop.getProperty("jobs"));
            for (int i = 0; i < jobs; i++){
                int readValue = Integer.parseInt(prop.getProperty(String.valueOf(i)));
                result.put(i, readValue/10);
            }
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            inputStream.close();
        }
        return result;
    }
}
