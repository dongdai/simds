package edu.dirlab.simds.gclient;

import org.apache.commons.cli.*;
import org.apache.thrift.TException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class ClientMain {

    Options options = new Options();

    //static final MetricRegistry metrics = new MetricRegistry();
    /*
     static {
     ConsoleReporter reporter = ConsoleReporter.forRegistry(metrics)
     .convertRatesTo(TimeUnit.SECONDS)
     .convertDurationsTo(TimeUnit.MILLISECONDS)
     .build();
     reporter.start(10, TimeUnit.SECONDS);
     }
    
     static final Histogram responseTime = metrics.histogram(MetricRegistry.name(ClientMain.class, "response-time"));
     */

    private void buildOptions() {
        // build option tables
        options.addOption(new Option("help", "print this message"));

        options.addOption(Option.builder("jid").hasArg()
                .desc("job id")
                .build());

        options.addOption(Option.builder("tid").hasArg()
                .desc("target server id")
                .build());

        options.addOption(Option.builder("bw").hasArg()
                .desc("bandwidth x op/s")
                .build());

        options.addOption(Option.builder("change").hasArg()
                .desc("change bandwidth")
                .build());

        options.addOption(Option.builder("srvlist").hasArgs()
                .desc("addresses of all servers")
                .build());
    }

    public String[] parseArgs(String[] args, List<String> allsrvs) {
        String[] rst = new String[4];
        CommandLineParser parser = new DefaultParser();
        try {
            // parse the command line arguments
            CommandLine line = parser.parse(options, args);

            if (args.length == 0) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("simds-client", options);
                System.exit(0);
            }

            if (line.hasOption("help")) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("simds-client", options);
                System.exit(0);
            }

            if (line.hasOption("jid")) {
                rst[0] = line.getOptionValue("jid");
            } else {
                throw new ParseException("argument 'jid' is required.");
            }

            if (line.hasOption("tid")) {
                rst[1] = line.getOptionValue("tid");
            } else {
                throw new ParseException("argument 'tid' is required.");
            }

            if (line.hasOption("bw")) {
                rst[2] = line.getOptionValue("bw");
            } else {
                throw new ParseException("argument 'bandwidth (bw)' is required.");
            }

            if (line.hasOption("change")) {
                rst[3] = line.getOptionValue("change");
            } else {
                throw new ParseException("argument 'change' is required.");
            }

            if (line.hasOption("srvlist")) {
                String[] srvs = line.getOptionValues("srvlist");
                allsrvs.addAll(Arrays.asList(srvs));
            }
        } catch (ParseException exp) {
            System.out.println("Arguments Error:" + exp.getMessage());
            System.exit(-1);
        }
        return rst;
    }

    public class IOTaskGenerator implements Runnable{
        int change;
        int alreadyRuns;
        int totalRun;
        Client client;
        int targetServer;
        byte[] src;
        byte[] val;

        public IOTaskGenerator(int change, int totalRun, Client client, int target, byte[] src, byte[] value){
            this.change = change;
            this.alreadyRuns = 0;
            this.totalRun = totalRun;
            this.client = client;
            this.targetServer = target;
            this.src = src;
            this.val = value;
        }
        @Override
        public void run() {
            try {
                //The actual bandwidth will be calculated on the server side.
                if (change == 1 && alreadyRuns >= (totalRun / 2)){
                    int r = alreadyRuns % 5;
                    alreadyRuns += 1;
                    if (r == 0) {
                        System.out.println("1. Generate workload to " + targetServer + " for job " + new String(src) + " at " + System.currentTimeMillis());
                        client.insert(targetServer, src, val);
                    }
                } else {
                    System.out.println("0. Generate workload to " + targetServer + " for job " + new String(src) + " at " + System.currentTimeMillis());
                    client.insert(targetServer, src, val);
                    alreadyRuns += 1;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void genWorkload(String[] args) throws InterruptedException {
        System.out.println("Start to generate workloads");
        buildOptions();
        ArrayList<String> allsrvs = new ArrayList<>();
        String[] rst = parseArgs(args, allsrvs);
        System.out.println("all servers: " + allsrvs);

        Client client = new Client(0, allsrvs);
        //Client client = null;  //just for local testing

        String jid = rst[0];
        String tid = rst[1];
        String bw = rst[2];
        int change = Integer.parseInt(rst[3]);

        //although each time we try to write an 1MB payload, we mimic what RDMA does, avoiding sending the actual
        //data to the server; the server will generate the payload
        final byte[] val = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".getBytes();
        final byte[] src = jid.getBytes();
        final int targetServer = Integer.parseInt(tid);
        final int BW_INTERNAL = (1000 / Integer.parseInt(bw));
        final int TOTAL_RUNTIME = 1000 * 30;

        System.out.println("Workload Job " + new String(src) + " target server: " + targetServer + " BW_INTERNAL: " + BW_INTERNAL);

        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        service.scheduleAtFixedRate(
                new IOTaskGenerator(change, TOTAL_RUNTIME / BW_INTERNAL, client, targetServer, src, val),
                0, BW_INTERNAL, TimeUnit.MILLISECONDS);

        Thread.sleep(TOTAL_RUNTIME);
        service.shutdown();
        System.out.println("Client Finishes");
    }

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Client Main Start");
        ClientMain cm = new ClientMain();
        cm.genWorkload(args);
    }
}
