package edu.dirlab.simds.gclient;

import edu.dirlab.simds.thrift.KeyValue;
import edu.dirlab.simds.thrift.TGraphFSServer;
import edu.dirlab.simds.utils.EdgeType;
import edu.dirlab.simds.utils.GLogger;
import org.apache.thrift.TException;
import org.apache.thrift.async.TAsyncClientManager;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class Client {
    public TGraphFSServer.Client[] conns;
    public TGraphFSServer.AsyncClient[] asyncClients;

    public ArrayList<String> allSrvs;
    public int port;
    public int serverNum;

    private static final Logger logger = LoggerFactory.getLogger(Client.class);

    public Client(int port, ArrayList<String> alls) {
        this.allSrvs = alls;
        this.port = port;
        this.serverNum = allSrvs.size();
        this.conns = new TGraphFSServer.Client[this.serverNum];
        this.asyncClients = new TGraphFSServer.AsyncClient[this.serverNum];

        for (int i = 0; i < this.serverNum; i++) {
            String addrPort = this.allSrvs.get(i);
            String addr = addrPort.split(":")[0];
            this.port = Integer.parseInt(addrPort.split(":")[1]);

            //TTransport transport = new TSocket(addr, port);
            TTransport transport = new TFramedTransport(new TSocket(addr, this.port),
                    1024 * 1024 * 1024);
            try {
                transport.open();
            } catch (TTransportException e) {
                GLogger.error("FATAL ERROR: Client can not connect to %s:%s", addr, this.port);
                System.exit(0);
            }
            TProtocol protocol = new TBinaryProtocol(transport);
            TGraphFSServer.Client client = new TGraphFSServer.Client(protocol);
            conns[i] = client;

            try {
                asyncClients[i] = new TGraphFSServer.AsyncClient(new TBinaryProtocol.Factory(),
                        new TAsyncClientManager(),
                        new TNonblockingSocket(addr, this.port));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public TGraphFSServer.Client getClientConn(int target) throws TTransportException {
        return conns[target];
    }

    public synchronized TGraphFSServer.AsyncClient getAsyncClientConn(int target) throws TTransportException {
        return asyncClients[target];
    }

    public List<KeyValue> read(int dstServer, byte[] srcKey) throws TException {
        byte[] dstKey = "bandguard".getBytes();
        return getClientConn(dstServer).read(ByteBuffer.wrap(srcKey), ByteBuffer.wrap(dstKey), EdgeType.OUT.get());
    }

    public int insert(int dstServer, byte[] key, byte[] value) throws TException {
        byte[] dstKey = "bandguard".getBytes();
        return getClientConn(dstServer).insert(ByteBuffer.wrap(key), ByteBuffer.wrap(dstKey),
                EdgeType.OUT.get(), ByteBuffer.wrap(value));
    }

    public List<KeyValue> scan(int dstServer, byte[] srcKey) throws TException {
        return getClientConn(dstServer).scan(ByteBuffer.wrap(srcKey), EdgeType.OUT.get());
    }
}
