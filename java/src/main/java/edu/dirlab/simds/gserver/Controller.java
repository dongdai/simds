package edu.dirlab.simds.gserver;

import edu.dirlab.simds.sengine.DBKey;
import edu.dirlab.simds.utils.EdgeType;
import edu.dirlab.simds.utils.TokenGetPropertyValues;

import edu.dirlab.simds.thrift.TGraphFSServer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.thrift.TException;

public class Controller {

    public Server instance = null;
    public int totalJobs = 0;
    public HashMap<Integer, Integer> tokensPerJob;
    public HashMap<Integer, Integer> tokenRatePerJob;
    public HashMap<Integer, Integer> bufferedIOsPerJob;
    public HashMap<Integer, Integer> IOPSPerJob;

    public Lock lock = null;
    public Condition cond = null;

    public final int MAX_TOKEN = 10;
    public final int TOKEN_INTERVAL = 100;
    public final int PRINT_INTERVAL = 2000;

    public Controller(Server s){
        this.instance = s;
        this.bufferedIOsPerJob = new HashMap<>();
        this.tokenRatePerJob = new HashMap<>();
        this.tokensPerJob = new HashMap<>();
        this.IOPSPerJob = new HashMap<>();

        this.lock = new ReentrantLock();
        this.cond = this.lock.newCondition();

        //load tokens per job.
        TokenGetPropertyValues tokensReader = new TokenGetPropertyValues();
        try {
            this.tokenRatePerJob = tokensReader.getTokens();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int j : this.tokenRatePerJob.keySet()){
            this.tokensPerJob.put(j, 0);
            this.bufferedIOsPerJob.put(j, 0);
            this.IOPSPerJob.put(j, 0);
        }
        this.totalJobs = this.tokensPerJob.size();
        System.out.println("Controller Initilization");
    }

    public void start(){
        System.out.println("Controller Start Working Threads");
        // io scheduler thread;
        new Thread(new IOSchedulerThreadRunnable(this)).start();

        // token generator: add 1 token to all jobs every 10 milliseconds.
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        service.scheduleAtFixedRate(new TokenGenerator(this), TOKEN_INTERVAL, TOKEN_INTERVAL, TimeUnit.MILLISECONDS);

        // output IOPS for each job
        ScheduledExecutorService outputService = Executors.newSingleThreadScheduledExecutor();
        outputService.scheduleAtFixedRate(new IOPSPrint(this), PRINT_INTERVAL, PRINT_INTERVAL, TimeUnit.MILLISECONDS);
    }

    public void scheduleIO(int jobId){
        this.lock.lock();
        try {
            if (this.tokensPerJob.containsKey(jobId)) {
                this.bufferedIOsPerJob.put(jobId, this.bufferedIOsPerJob.get(jobId) + 1);
                this.cond.signal();
            } else {
                System.out.println("Illegal Job Id: " + jobId);
            }
        } finally {
            this.lock.unlock();
        }
    }

    public class IOPSPrint implements Runnable{
        private Controller c = null;
        public IOPSPrint(Controller c){
            this.c = c;
        }
        @Override
        public void run() {
            c.lock.lock();
            if (true) { //only server 0 prints instance.getLocalIdx() == 1 || instance.getLocalIdx() == -1
                System.out.print("S-" + instance.getLocalIdx());
                for (int j : c.IOPSPerJob.keySet()) {  //
                    System.out.print("  (job-" + j + "):<" + ((c.IOPSPerJob.get(j) + 0.0) / (PRINT_INTERVAL / 1000 + 0.0)) + ">[" + c.tokensPerJob.get(j) + "]{" + c.bufferedIOsPerJob.get(j) + "},");
                    c.IOPSPerJob.put(j, 0);
                }
                System.out.println("");
            }
            c.lock.unlock();
        }
    }

    public class TokenGenerator implements Runnable{
        private Controller c = null;
        public TokenGenerator(Controller c){
            this.c = c;
        }
        @Override
        public void run() {
            c.lock.lock();
            for (int j : c.tokensPerJob.keySet()){
                if (c.tokensPerJob.get(j) >= MAX_TOKEN * c.tokenRatePerJob.get(j))
                    continue;
                c.tokensPerJob.put(j, c.tokensPerJob.get(j) + c.tokenRatePerJob.get(j));
            }
            c.lock.unlock();
        }
    }

    public class IOSchedulerThreadRunnable implements Runnable{
        private Controller c = null;
        public IOSchedulerThreadRunnable(Controller c){
            this.c = c;
        }

        private boolean hasBufferedIOs(){
            for (int ios : c.bufferedIOsPerJob.values()) {
                if (ios > 0)
                    return true;
            }
            return false;
        }

        @Override
        public void run() {
            while (true){
                c.lock.lock();
                try {
                    try {
                        while (!hasBufferedIOs())
                            c.cond.await();
                    } catch (InterruptedException ie){
                        c.cond.signal();
                    }
                } finally {
                    c.lock.unlock();
                }

                c.lock.lock();

                // pick a job to perform IO
                int pickedJob = -1;

                int total_token_rate = 0;
                int total_token_active = 0;
                ArrayList<Integer> jobsWithTokensAndIOs = new ArrayList<>();
                ArrayList<Integer> jobsWithIOs = new ArrayList<>();

                HashMap<Integer, Integer> tokensNeededPerJob = new HashMap<>();

                long token_time = System.currentTimeMillis();

                for (int j : tokensPerJob.keySet()){
                    int t = tokensPerJob.get(j);
                    int io = bufferedIOsPerJob.get(j);
                    int rate = tokenRatePerJob.get(j);
                    if (io > 0) {
                        total_token_rate += rate;
                        jobsWithIOs.add(j);
                    }
                    if (t > 0 && io > 0) {
                        total_token_active += t;
                        jobsWithTokensAndIOs.add(j);
                    }
                    // collect all jobs who need to borrow
                    if (io > 0 && t <= 0){
                        tokensNeededPerJob.put(j, io - t - rate);
                    }
                }

                // try to borrow tokens, all requests are combined together
                if (true) {  //!tokensNeededPerJob.isEmpty()
                    HashMap<Integer, Integer> total_borrowed = new HashMap<>();
                    for (int i = 0; i < this.c.instance.serverNum; i++) {
                        Map<Integer, Integer> borrowed = new HashMap<>();
						try {
                            TGraphFSServer.Client client = this.c.instance.getClientConn(i);
                        	borrowed = client.borrow_tokens(tokensNeededPerJob);
						} catch (TException e) {
							e.printStackTrace();
						}
                        for (int j : borrowed.keySet()){
                            int t = borrowed.get(j);
                            if (!total_borrowed.containsKey(j))
                                total_borrowed.put(j, 0);
                            total_borrowed.put(j, total_borrowed.get(j) + t);
                        }
                    }
                    for (int j : total_borrowed.keySet()){
                        int t = total_borrowed.get(j);
                        total_token_active += t;
                        jobsWithTokensAndIOs.add(j);
                        tokensPerJob.put(j, tokensPerJob.get(j) + t);
                    }
                }


                // update total_token_active and jobsWithTokensAndIOs if job i borrows something.

                if (jobsWithTokensAndIOs.size() > 0){
                    double randomPick = new Random().nextDouble() * (total_token_active + 0.0);
                    for (int j : jobsWithTokensAndIOs){
                        int t = tokensPerJob.get(j);
                        if (randomPick < t) {
                            pickedJob = j;
                            break;
                        } else {
                            randomPick -= t;
                        }
                    }
                } else {
                    double randomPick = new Random().nextDouble() * (total_token_rate + 0.0);
                    for (int j : jobsWithIOs){
                        int t = tokenRatePerJob.get(j);
                        if (randomPick < t){
                            pickedJob = j;
                            break;
                        } else {
                            randomPick -= t;
                        }
                    }
                }

                if (pickedJob == -1) {
                    System.out.println("Error");
                    c.lock.unlock();
                    continue;
                }

                c.bufferedIOsPerJob.put(pickedJob, Math.max(0, c.bufferedIOsPerJob.get(pickedJob) - 1));
                c.tokensPerJob.put(pickedJob, Math.max(0, c.tokensPerJob.get(pickedJob) - 1));
                c.IOPSPerJob.put(pickedJob, c.IOPSPerJob.get(pickedJob) + 1);

                c.lock.unlock();
                System.out.println("Token Op takes: " + (System.currentTimeMillis() - token_time));

                // perform the real IO
                String dstKeyString = "bandguard" + new Random().nextInt();
                byte[] dstKey = dstKeyString.getBytes();
                final byte[] bval = c.instance.payload128K.getBytes();
                byte[] pickedJobBytes = String.valueOf(pickedJob).getBytes();
                DBKey newKey = new DBKey(pickedJobBytes, dstKey, EdgeType.OUT.get());
                //instance.localStore.put(newKey.toKey(), bval);

                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //System.out.println("Perform IO for job " + pickedJob);
            }
        }
    }

    public static void main(String[] args){
        //test
        final Server s = new Server();
        final Controller c = new Controller(s);
        c.start();

        Runnable ioTask = new Runnable() {
            @Override
            public void run() {
                //int jobId = new Random().nextInt(c.totalJobs);
                c.scheduleIO(1);
                c.scheduleIO(2);
            }
        };
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        service.scheduleAtFixedRate(ioTask, 0, 10, TimeUnit.MILLISECONDS);
    }
}
