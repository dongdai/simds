package edu.dirlab.simds.gserver;

import java.util.ArrayList;
import java.util.List;

public class ServerThreadWrapper implements Runnable {

    private String db;
    private int localIdx = -1;
    private List<String> allServers = new ArrayList<String>() {
        {
            //add("");
        }
    };

    public ServerThreadWrapper(String db, int localIdx, List<String> allServers) {
        this.db = db;
        this.localIdx = localIdx;
        this.allServers = allServers;
    }

    private void initialCheck() {
        if (db == null || localIdx < 0 || allServers.isEmpty()) {
            System.err.println("type, dbnum, idx or allServers are not well-configured. Please check the parameter.");
            System.exit(1);
        }
    }

    @Override
    public void run() {
        initialCheck();
        Server abstractSrv = new Server();
        abstractSrv.setDbFile(this.getDb());
        abstractSrv.setLocalIdx(this.getLocalIdx());
        abstractSrv.setAllSrvs(this.getAllServers());
        abstractSrv.runit();
    }

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public int getLocalIdx() {
        return localIdx;
    }

    public void setLocalIdx(int localIdx) {
        this.localIdx = localIdx;
    }

    public List<String> getAllServers() {
        return allServers;
    }

    public void setAllServers(List<String> allServers) {
        this.allServers = allServers;
    }
}
