package edu.dirlab.simds.gserver;

import edu.dirlab.simds.sengine.OrderedRocksDBAPI;
import edu.dirlab.simds.thrift.TGraphFSServer;
import edu.dirlab.simds.utils.GLogger;
import edu.dirlab.simds.utils.JenkinsHash;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadedSelectorServer;
import org.apache.thrift.transport.*;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Server {

    public TGraphFSServer.Iface handler;
    public TGraphFSServer.Processor processor;

    /**
     * Synchronous Clients
     */
    public HashMap<Integer, ConcurrentLinkedQueue<TGraphFSServer.Client>> clientPools;

    private String dbFile = null;
    private int localIdx = -1;
    private List<String> allSrvs = new ArrayList<>();

    public String localAddr;
    public int port;
    public int serverNum;
    public OrderedRocksDBAPI localStore;

    public String payload128K;

    public Controller controller;

    //Monitoring
    //public final MetricRegistry METRICS = new MetricRegistry();

    public Server() {
        int procs = Runtime.getRuntime().availableProcessors();
        procs = Math.max(procs, 1);
        this.handler = new ServerHandler(this);
        this.processor = new TGraphFSServer.Processor(this.handler);
        /*
         ConsoleReporter reporter = ConsoleReporter.forRegistry(METRICS)
         .convertRatesTo(TimeUnit.SECONDS)
         .convertDurationsTo(TimeUnit.MILLISECONDS)
         .build();
         reporter.start(10, TimeUnit.SECONDS);
         */

        this.controller = new Controller(this);
        StringBuilder sb32bytes = new StringBuilder("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        for (int i = 0; i < 4096; i++) sb32bytes.append("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        this.payload128K = sb32bytes.toString();
    }

    public void runit() {
        init();
        start();
    }

    public void init() {
        if (dbFile == null || localIdx < 0 || allSrvs.isEmpty()) {
            System.err.println("dbnum, idx or allsrvs are not well-configured. " +
                    "Please check the parameter.");
            System.exit(1);
        }
        String localAddrPort = this.getAllSrvs().get(this.getLocalIdx());
        this.localAddr = localAddrPort.split(":")[0];
        this.port = Integer.parseInt(localAddrPort.split(":")[1]);
        this.serverNum = this.getAllSrvs().size();
        this.localStore = new OrderedRocksDBAPI(this.getDbFile());

        /**
         * Sync Clients
         */
        this.clientPools = new HashMap<>(this.serverNum);
        for (int i = 0; i < this.serverNum; i++)
            this.clientPools.put(i, new ConcurrentLinkedQueue<TGraphFSServer.Client>());
    }

    public void start() {
        try {
            //About Thrift server: http://www.voidcn.com/article/p-xpdesbbf-ks.html
            TNonblockingServerSocket serverTransport = new TNonblockingServerSocket(this.port);
            TThreadedSelectorServer.Args tArgs = new TThreadedSelectorServer.Args(serverTransport);
            tArgs.processor(processor);
            tArgs.transportFactory(new TFramedTransport.Factory(1024 * 1024 * 1024));
            tArgs.protocolFactory(new TBinaryProtocol.Factory());
            TServer server = new TThreadedSelectorServer(tArgs);

            /* TThreadedSelectorServer is the latest server of Thrift.
            TServerSocket serverSocket = new TServerSocket(this.port);
            TThreadPoolServer.Args tArgs = new TThreadPoolServer.Args(serverSocket);
            tArgs.processor(processor);
            tArgs.protocolFactory(new TBinaryProtocol.Factory());
            TServer server = new TThreadPoolServer(tArgs);
            */

            this.controller.start();

            GLogger.info("[%d] Starting SimDS Server at %s:%d", this.getLocalIdx(), this.localAddr, this.port);
            server.serve();

        } catch (TException e) {
            e.printStackTrace();
        }
    }

    public TGraphFSServer.Client getClientConn(int target) throws TTransportException {

        synchronized (this.clientPools) {
            if (!this.clientPools.containsKey(target))
                this.clientPools.put(target, new ConcurrentLinkedQueue<TGraphFSServer.Client>());
        }

        ConcurrentLinkedQueue<TGraphFSServer.Client> pool = this.clientPools.get(target);

        synchronized (pool) {
            if (pool.isEmpty()) {
                String addrPort = this.getAllSrvs().get(target);
                String taddr = addrPort.split(":")[0];
                int tport = Integer.parseInt(addrPort.split(":")[1]);
                TTransport transport = new TFramedTransport(new TSocket(taddr, tport), 2 * 1024 * 1024);
                transport.open();
                TProtocol protocol = new TBinaryProtocol(transport);
                TGraphFSServer.Client client = new TGraphFSServer.Client(protocol);
                return client;
            } else {
                TGraphFSServer.Client client = pool.poll(); //retrieve and remove the first element
                return client;
            }
        }
    }

    public boolean releaseClientConn(int target, TGraphFSServer.Client c){
        ConcurrentLinkedQueue<TGraphFSServer.Client> pool = this.clientPools.get(target);
        synchronized (pool) {
            pool.add(c);
        }
        return true;
    }

    public String getDbFile() {
        return dbFile;
    }

    public void setDbFile(String dbFile) {
        this.dbFile = dbFile;
    }

    public int getLocalIdx() {
        return localIdx;
    }

    public void setLocalIdx(int localIdx) {
        this.localIdx = localIdx;
    }

    public List<String> getAllSrvs() {
        return allSrvs;
    }

    public void setAllSrvs(List<String> allSrvs) {
        this.allSrvs = allSrvs;
    }

    public int getHashLocation(byte[] src, int serverNum) {
        JenkinsHash jh = new JenkinsHash();
        int hashi = Math.abs(jh.hash32(src));
        return (hashi % serverNum);
    }

    public Set<Integer> getEdgeLocs(byte[] src, int type) {
        Set<Integer> locs = new HashSet<>();
        int startIdx = getHashLocation(src, this.serverNum);
        locs.add(startIdx);
        return locs;
    }
}
