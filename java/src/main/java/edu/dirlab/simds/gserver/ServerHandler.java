package edu.dirlab.simds.gserver;

import edu.dirlab.simds.sengine.DBKey;
import edu.dirlab.simds.utils.Constants;
import edu.dirlab.simds.utils.NIOHelper;
import edu.dirlab.simds.thrift.KeyValue;
import edu.dirlab.simds.thrift.TGraphFSServer;
import org.apache.thrift.TException;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServerHandler implements TGraphFSServer.Iface {

    public Server instance = null;

    public ServerHandler(Server s) {
        this.instance = s;
    }

    @Override
    public int echo(int s, ByteBuffer payload) throws TException {
        byte[] load = NIOHelper.getActiveArray(payload);
        System.out.println("[Thrift Test] Server Receive: " + " Int: " + s + " ByteBuffer: " + new String(load));
        return 0;
    }

    @Override
    public int insert(ByteBuffer src, ByteBuffer dst, int type, ByteBuffer val) throws TException {
        byte[] jobId = NIOHelper.getActiveArray(src);
        // System.out.println("Sever " + instance.getLocalIdx() + " Receives Insert
        // Request for Job " + new String(jobId));
        instance.controller.scheduleIO(Integer.parseInt(new String(jobId)));
        return 0;
    }

    @Override
    public synchronized List<KeyValue> read(ByteBuffer src, ByteBuffer dst, int type) throws TException {
        byte[] bsrc = NIOHelper.getActiveArray(src);
        byte[] bdst = NIOHelper.getActiveArray(dst);

        List<KeyValue> rtn = new ArrayList<KeyValue>();
        DBKey newKey = new DBKey(bsrc, bdst, type);
        KeyValue p = instance.localStore.seekTo(newKey.toKey());
        if (p != null) {
            rtn.add(p);
        }
        return rtn;
    }

    @Override
    public synchronized List<KeyValue> scan(ByteBuffer src, int type) throws TException {
        byte[] bsrc = NIOHelper.getActiveArray(src);

        DBKey startKey = DBKey.MinDBKey(bsrc, type);
        DBKey endKey = DBKey.MaxDBKey(bsrc, type);
        ArrayList<KeyValue> kvs = instance.localStore.scanKV(startKey.toKey(), endKey.toKey());

        return kvs;
    }

    @Override
    public int batch_insert(List<KeyValue> batches, int type) throws TException {
        instance.localStore.batch_put(batches);
        return Constants.RTN_SUCC;
    }

    @Override
    public Map<Integer, Integer> borrow_tokens(Map<Integer, Integer> t) throws TException {
        HashMap<Integer, Integer> rtn = new HashMap<>();
        return rtn;
    }
}
