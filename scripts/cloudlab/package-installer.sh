#!/usr/bin/env bash

sudo apt-get update
sudo apt-get install -y openjdk-8-jdk
sudo apt-get install -y thrift-compiler
sudo apt-get install -y maven
sudo apt-get install -y libgflags-dev
sudo apt-get install -y libsnappy-dev

cd ~/
git clone https://dongdai@bitbucket.org/dongdai/simds.git
cd ~/simds
make all

id=1
hdd="/dev/sdb"
for i in $hdd;do
echo "n
p
1


w
"|sudo fdisk $i; sudo mkfs.ext3 $i$id; done

sudo mkdir -p /media/disk1
sudo mount /dev/sdb$id /media/disk1
sudo chown -R dirruncc:dirr-PG0 /media/disk1

mkdir -p /media/disk1/dbs/

echo "export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64/" >> ~/.bashrc