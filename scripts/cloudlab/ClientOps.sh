#!/usr/bin/env bash

while [[ $# > 1 ]]
do
    key="$1"

    case $key in
        -n | --number)
            server_number=$2
            shift
            ;;
        -j | --jid)
	    	jid="$2"
	    	shift
	    	;;
		-t | --tid)
            tid="$2"
            shift
            ;;
		-b | --bw)
	    	bw="$2"
	    	shift
	    	;;
        -c | --change)
	    	change="$2"
	    	shift
	    	;;
        --default)
            echo default
            ;;
        *)
            ;;
    esac
    shift
done

bound=`expr ${server_number} - 1`

port=5555
seeds=""
line=0
for i in $(seq 0 $bound)
do
    if [ "$line" -eq 0 ]
    then
        seeds="node-$i:$port"
    else
        seeds="$seeds node-$i:$port"
    fi
    line=`expr 1 + $line`
done

echo "~/simds/release/simds-0.2/bin/client.sh -jid $jid -tid $tid -bw $bw -change $change -srvlist $seeds"
~/simds/release/simds-0.2/bin/client.sh -jid $jid -tid $tid -bw $bw -change $change -srvlist $seeds
