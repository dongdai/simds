#!/usr/bin/env bash

# workload case 2: 4 applications, proportially share the bandwidth
# -b 100 means every 10ms send out 1MB request
# application 4 reduces bandwidth to half 
~/simds/scripts/cloudlab/ClientOps.sh -n 3 -j 0 -t 1 -b 50 -c 0 &

~/simds/scripts/cloudlab/ClientOps.sh -n 3 -j 1 -t 1 -b 50 -c 0 &

~/simds/scripts/cloudlab/ClientOps.sh -n 3 -j 2 -t 1 -b 50 -c 0 &

~/simds/scripts/cloudlab/ClientOps.sh -n 3 -j 3 -t 1 -b 50 -c 1 &