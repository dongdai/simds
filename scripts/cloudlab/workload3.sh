#!/usr/bin/env bash

# workload case 3: global borrow
~/simds/scripts/cloudlab/ClientOps.sh -n 3 -j 0 -t 0 -b 60 -c 0 &
~/simds/scripts/cloudlab/ClientOps.sh -n 3 -j 0 -t 1 -b 20 -c 0 &
~/simds/scripts/cloudlab/ClientOps.sh -n 3 -j 0 -t 2 -b 20 -c 0 &

~/simds/scripts/cloudlab/ClientOps.sh -n 3 -j 1 -t 0 -b 60 -c 0 &
