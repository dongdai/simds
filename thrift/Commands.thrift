namespace java edu.dirlab.simds.thrift

struct KeyValue {
  1: binary key,
  2: binary value,
}

service TGraphFSServer {
    i32 echo(1:i32 s, 2:binary payload),
    i32 insert(1:binary src, 2:binary dst, 3:i32 type, 4:binary val),
    i32 batch_insert(1:list<KeyValue> batches, 2:i32 type),
    list<KeyValue> read(1:binary src, 2:binary dst, 3:i32 type),
    list<KeyValue> scan(1:binary src, 2:i32 type), 
    map<i32, i32> borrow_tokens(1: map<i32,i32> t),
}